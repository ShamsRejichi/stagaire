package com.bezkoder.spring.jwt.mongodb.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bezkoder.spring.jwt.mongodb.models.Consultant;

public interface ConsultantRepository extends MongoRepository<Consultant, String> {
  Optional<Consultant> findByFirstName(String firstName);
  Optional<Consultant> findByLastName(String lastName);
  Consultant findByEmail(String email);

  List<Consultant> findAll();

  void deleteById(int id);
  
  Boolean existsByFirstName(String firstName);
  Boolean existsByLastName(String lastName);

  Boolean existsByEmail(String email);
}
