package com.bezkoder.spring.jwt.mongodb.payload.request;

import javax.validation.constraints.*;



 
public class DeleteConsultantRequest {
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }

    public void deleteById(String email) {
    
    }
 
}
