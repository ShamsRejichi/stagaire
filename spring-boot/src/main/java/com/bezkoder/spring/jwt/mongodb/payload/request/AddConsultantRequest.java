package com.bezkoder.spring.jwt.mongodb.payload.request;

import javax.validation.constraints.*;
 
public class AddConsultantRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String firstName;
 
    
    @NotBlank
    @Size(min = 3, max = 20)
    private String lastName;
 
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 50)
    private String poste;
    
    
    @NotBlank
    @Size(max = 50)
    private String experience;

    @NotBlank
    @Size(max = 50)
    private String numero;

    
    @NotBlank
    @Size(max = 150)
    private String adresse;

    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPoste() {
        return poste;
    }
 
    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getExperience() {
        return experience;
    }
 
    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getAdresse() {
        return adresse;
    }
 
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumero() {
        return numero;
    }
 
    public void setNumero(String numero) {
        this.numero = numero;
    }
 
 
}
