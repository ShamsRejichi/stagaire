package com.bezkoder.spring.jwt.mongodb.models;


import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "compterendu")
public class CompteRendu {

    
  @Id
  private String id;

  
  @NotBlank
  @Size(max = 20)
  private String clientName;

  @NotBlank
  @Size(max = 20)
  private String firstName;

  
  @NotBlank
  @Size(max = 20)
  private String lastName;

  
  @NotBlank
  @Size(max = 50)
  private String email;

  
  @NotBlank
  @Size(max = 50)
  private String poste;
  
  
  @NotBlank
  @Size(max = 50)
  private String experience;

  @NotBlank
  @Size(max = 50)
  private String numero;

  
  @NotBlank
  @Size(max = 50)
  private String intershipDuration;

  
  @Size(max = 50)
  private String intershipType;

  
  @Size(max = 50)
  private String intershipBTW;
  
  @NotBlank
  @Size(max = 150)
  private String adresse;

  
  @NotBlank
  @Size(max = 50)
  private String numberOfParticipant;

  
  @Size(max = 50)
  private String positionnement;

  @NotBlank
  private Date date;

  

  public CompteRendu() {
  }

  public CompteRendu(String clientName,String firstName, String lastName,String email,String poste,String numero,String adresse,String intershipDuration,String numberOfParticipant,String intershipBTW,String intershipType,String positionnement,Date date) {
    this.clientName = clientName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.poste = poste;
    this.numero = numero;
    this.adresse = adresse;
    this.intershipDuration = intershipDuration;
    this.numberOfParticipant = numberOfParticipant ;
    this.intershipBTW = intershipBTW;
    this.intershipType = intershipType; 
    this.positionnement = positionnement;
    this.date = date;
      }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  
  public String getPoste() {
    return poste;
}

public void setPoste(String poste) {
    this.poste = poste;
}

public String getAdresse() {
    return adresse;
}

public void setAdresse(String adresse) {
    this.adresse = adresse;
}

public String getNumero() {
    return numero;
}

public void setNumero(String numero) {
    this.numero = numero;
}
 

public String getIntershipDuration() {
    return intershipDuration;
}

public void setIntershipDuration(String intershipDuration) {
    this.intershipDuration = intershipDuration;
}

public String getIntershipType() {
    return intershipType;
}

public void setIntershipType(String intershipType) {
    this.intershipType = intershipType;
}

public String getIntershipBTW() {
    return intershipBTW;
}

public void setIntershipBTW(String intershipBTW) {
    this.intershipBTW = intershipBTW;
}


public String getNumberOfParticipant() {
    return numberOfParticipant;
}

public void setNumberOfParticipant(String numberOfParticipant) {
    this.numberOfParticipant = numberOfParticipant;
}

public String getPositionnement() {
  return positionnement;
}

public void setPositionnement(String positionnement) {
  this.positionnement = positionnement;
}

public Date getDate() {
  return date;
}

public void setDate(Date date) {
  this.date = date;
}
}
