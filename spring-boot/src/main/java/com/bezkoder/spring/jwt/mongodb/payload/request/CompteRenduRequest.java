package com.bezkoder.spring.jwt.mongodb.payload.request;

import java.sql.Date;

import javax.validation.constraints.*;


public class CompteRenduRequest {
  
  @NotBlank
  @Size(max = 20)
  private String clientName;

  @NotBlank
  @Size(max = 20)
  private String firstName;

  
  @NotBlank
  @Size(max = 20)
  private String lastName;

  
  @NotBlank
  @Size(max = 50)
  private String poste;

  
  
  @NotBlank
  @Size(max = 50)
  private String adress;

  
  
  @NotBlank
  @Size(max = 150)
  @Email
  private String email;
  

  @NotBlank
  @Size(max = 50)
  private String numero;

  
  @NotBlank
  @Size(max = 50)
  private String intershipDuration;

  
  @Size(max = 50)
  private String intershipType;

  
  @Size(max = 50)
  private String intershipBTW;
  
  @NotBlank
  @Size(max = 50)
  private String numberOfParticipant;

  
  @Size(max = 50)
  private String positionnement;

  private Date date;


  
  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  
  public String getPoste() {
    return poste;
}

public void setPoste(String poste) {
    this.poste = poste;
}

  
public String getEmail() {
  return email;
}

public void setEmail(String email) {
  this.email = email;
}

  
public String getAdress() {
  return adress;
}

public void setAdress(String adress) {
  this.adress = adress;
}

public String getNumero() {
    return numero;
}

public void setNumero(String numero) {
    this.numero = numero;
}
 

public String getIntershipDuration() {
    return intershipDuration;
}

public void setIntershipDuration(String intershipDuration) {
    this.intershipDuration = intershipDuration;
}

public String getIntershipType() {
    return intershipType;
}

public void setIntershipType(String intershipType) {
    this.intershipType = intershipType;
}

public String getIntershipBTW() {
    return intershipBTW;
}

public void setIntershipBTW(String intershipBTW) {
    this.intershipBTW = intershipBTW;
}


public String getNumberOfParticipant() {
    return numberOfParticipant;
}

public void setNumberOfParticipant(String numberOfParticipant) {
    this.numberOfParticipant = numberOfParticipant;
}


public String getPositionnement() {
  return positionnement;
}

public void setPositionnement(String positionnement) {
  this.positionnement = positionnement;
}

public Date getDate() {
  return date;
}

public void setDate(Date date) {
  this.date = date;
}
}
