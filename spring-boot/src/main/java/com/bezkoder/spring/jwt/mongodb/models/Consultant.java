package com.bezkoder.spring.jwt.mongodb.models;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "consultants")
public class Consultant {
  @Id
  private String id;

  @NotBlank
  @Size(max = 20)
  private String firstName;

  
  @NotBlank
  @Size(max = 20)
  private String lastName;


  @NotBlank
  @Size(max = 50)
  @Email
  private String email;

  
  @NotBlank
  @Size(max = 50)
  private String poste;
  
  
  @NotBlank
  @Size(max = 50)
  private String experience;

  @NotBlank
  @Size(max = 50)
  private String numero;

  
  @NotBlank
  @Size(max = 150)
  private String adresse;


  public Consultant() {
  }

  public Consultant(String firstName, String lastName, String email,String poste,String experience,String numero,String adresse) {
    this.firstName = firstName;
    this.email = email;
    this.lastName = lastName;
    this.poste = poste;
    this.experience = experience;
    this.numero = numero;
    this.adresse = adresse;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  
  public String getPoste() {
    return poste;
}

public void setPoste(String poste) {
    this.poste = poste;
}

public String getExperience() {
    return experience;
}

public void setExperience(String experience) {
    this.experience = experience;
}

public String getAdresse() {
    return adresse;
}

public void setAdresse(String adresse) {
    this.adresse = adresse;
}

public String getNumero() {
    return numero;
}

public void setNumero(String numero) {
    this.numero = numero;
}
}
