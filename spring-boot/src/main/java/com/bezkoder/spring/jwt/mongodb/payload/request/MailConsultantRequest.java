package com.bezkoder.spring.jwt.mongodb.payload.request;

import javax.validation.constraints.*;
 
public class MailConsultantRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String firstName;
 
    
    @NotBlank
    @Size(min = 3, max = 20)
    private String lastName;
 
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;


    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
 
}
