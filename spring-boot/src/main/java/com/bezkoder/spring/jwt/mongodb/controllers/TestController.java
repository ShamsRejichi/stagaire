package com.bezkoder.spring.jwt.mongodb.controllers;

import java.util.List;

import javax.validation.Valid;

import com.bezkoder.spring.jwt.mongodb.models.CompteRendu;
import com.bezkoder.spring.jwt.mongodb.models.Consultant;
import com.bezkoder.spring.jwt.mongodb.payload.request.AddConsultantRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.CompteRenduRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.DeleteConsultantRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.MailConsultantRequest;
import com.bezkoder.spring.jwt.mongodb.payload.response.MessageResponse;
import com.bezkoder.spring.jwt.mongodb.repository.CompteRenduRepository;
import com.bezkoder.spring.jwt.mongodb.repository.ConsultantRepository;
import com.bezkoder.spring.jwt.mongodb.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {

	
    @Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ConsultantRepository consultantRepository;

	
	@Autowired
	CompteRenduRepository compteRenduRepository;

	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}
	
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public String userAccess() {
		return "User Content.";
	}

	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}

	@GetMapping("/users")
	public List<Consultant> getUsers() {
		return consultantRepository.findAll();
	}	

	
	@PostMapping("/delete")
	public ResponseEntity<?> deleteConsultant(@Valid @RequestBody DeleteConsultantRequest deleteConsultantRequest) {
		Consultant consultant = consultantRepository.findByEmail(deleteConsultantRequest.getEmail());
		consultantRepository.deleteById(consultant.getId());
		return ResponseEntity.ok(new MessageResponse( "Consultant" + consultant.getFirstName() +"deleted successfully!"));
	}

	
	@PostMapping("/updateConsultant")
	public ResponseEntity<?> updateConsultant(@Valid @RequestBody AddConsultantRequest addConsultantRequest) {
		// update	 new Consultant
		Consultant consultant = consultantRepository.findByEmail(addConsultantRequest.getEmail());
		consultant.setFirstName(addConsultantRequest.getFirstName());
		consultant.setLastName(addConsultantRequest.getLastName());
		consultantRepository.save(consultant);

		return ResponseEntity.ok(new MessageResponse("Consultant" + consultant.getFirstName() + "Updated successfully!"));
	}


		
	@PostMapping("/sendMailConsultant")
	public ResponseEntity<?> sendMailConsultant(@Valid @RequestBody MailConsultantRequest sendConsultantRequest) {

		if(userRepository.existsByEmail(sendConsultantRequest.getEmail())){
			
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(sendConsultantRequest.getEmail());
			msg.setSubject("Saisie de compte rendu");
			msg.setText("Bonjour "+ sendConsultantRequest.getFirstName()+" " + sendConsultantRequest.getLastName() +",Nous vous prions de bien vouloir saisir votre compte rendu suite à l’entretien partenaire/client que vous avez eu http://localhost:4200/fromConsultant");

			javaMailSender.send(msg);
		}else{
			
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(sendConsultantRequest.getEmail());
			msg.setSubject("Création de compte");
			msg.setText("Bonjour "+ sendConsultantRequest.getFirstName()+" " + sendConsultantRequest.getLastName() +", nous vous  invitons à créer votre compte CR Consultant sur l’application http://localhost:4200/register. Vous serez amenés à saisir vos éventuels comptes rendus suite aux entretiens clients/partenaires");
	
			javaMailSender.send(msg);
		}
		
		
		return ResponseEntity.ok(new MessageResponse("Consultant Mail Sended!"));
	}


	@PostMapping("/addCompteRendu")
	public ResponseEntity<?> addCompteRendu(@Valid @RequestBody CompteRenduRequest compteRenduRequest) {
	
			CompteRendu compteRendu = new CompteRendu(compteRenduRequest.getClientName(),
			compteRenduRequest.getFirstName(), 
			compteRenduRequest.getLastName(),
			compteRenduRequest.getEmail(),
			compteRenduRequest.getPoste(),
			compteRenduRequest.getNumero(),
			compteRenduRequest.getAdress(),
			compteRenduRequest.getIntershipDuration(),
			compteRenduRequest.getNumberOfParticipant(),
			compteRenduRequest.getIntershipBTW(),
			compteRenduRequest.getIntershipType(),
			compteRenduRequest.getPositionnement(),
			compteRenduRequest.getDate());

			compteRenduRepository.save(compteRendu);

			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo("shams.rejichi@gmail.com");
			msg.setSubject("Creation compte rendu");
			msg.setText(compteRenduRequest.getFirstName()+" "+compteRenduRequest.getLastName()+" à créer un compte rendu");
			javaMailSender.send(msg);

		
		return ResponseEntity.ok(new MessageResponse("compteRendu Created!"));
	}


	
	@PostMapping("/getCompteRendu")
	public List<CompteRendu>  getCompteRendu(@Valid @RequestBody DeleteConsultantRequest getcompteRenduRequest) {
	 return compteRenduRepository.findByEmail(getcompteRenduRequest.getEmail());
	}
}	



