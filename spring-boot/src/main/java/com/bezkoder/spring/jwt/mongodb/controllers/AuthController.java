package com.bezkoder.spring.jwt.mongodb.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.spring.jwt.mongodb.models.Consultant;
import com.bezkoder.spring.jwt.mongodb.models.ERole;
import com.bezkoder.spring.jwt.mongodb.models.Role;
import com.bezkoder.spring.jwt.mongodb.models.User;
import com.bezkoder.spring.jwt.mongodb.payload.request.AddConsultantRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.LoginRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.NewPasswordRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.ResetPasswordRequest;
import com.bezkoder.spring.jwt.mongodb.payload.request.SignupRequest;
import org.springframework.mail.javamail.JavaMailSender;
import com.bezkoder.spring.jwt.mongodb.payload.response.JwtResponse;
import com.bezkoder.spring.jwt.mongodb.payload.response.MessageResponse;
import com.bezkoder.spring.jwt.mongodb.repository.RoleRepository;
import com.bezkoder.spring.jwt.mongodb.repository.UserRepository;
import com.bezkoder.spring.jwt.mongodb.repository.ConsultantRepository;
import com.bezkoder.spring.jwt.mongodb.security.jwt.JwtUtils;
import com.bezkoder.spring.jwt.mongodb.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

		
    @Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ConsultantRepository consultantRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		if (!userRepository.existsByEmail(loginRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Use dose not exist!"));
		}

		User user = userRepository.findByEmail(loginRequest.getEmail());

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(user.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}


		if (signUpRequest.getEmail().indexOf("redlean.io") == -1 ) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Format mail is incorrect!"));
		}


		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRoles();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}


	@PostMapping("/addConsultant")
	public ResponseEntity<?> addConsultant(@Valid @RequestBody AddConsultantRequest addConsultantRequest) {
		if (consultantRepository.existsByFirstName(addConsultantRequest.getFirstName())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: FirstName is already taken!"));
		}

		if (consultantRepository.existsByLastName(addConsultantRequest.getLastName())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: LastName is already taken!"));
		}

		if (consultantRepository.existsByEmail(addConsultantRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new Consultant
		Consultant consultant = new Consultant(addConsultantRequest.getFirstName(), addConsultantRequest.getLastName(),addConsultantRequest.getEmail(),addConsultantRequest.getPoste(),addConsultantRequest.getExperience(),addConsultantRequest.getNumero(),addConsultantRequest.getAdresse());

		consultantRepository.save(consultant);

		return ResponseEntity.ok(new MessageResponse("Consultant registered successfully!"));
	}

	
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping("/resetPassword")
	public ResponseEntity<?> registerUser(@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) {

		if(userRepository.existsByEmail(resetPasswordRequest.getEmail())){
			
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(resetPasswordRequest.getEmail());
			msg.setSubject("Reset Password");
			msg.setText("Bonjour ,reset ton mot de passe: http://localhost:4200/resetPassword");

			javaMailSender.send(msg);
		}


		return ResponseEntity.ok(new MessageResponse("User Reset Password was successfully!"));
	}


	
	
	@PostMapping("/newpassword")
	public ResponseEntity<?> newPasswordUser(@Valid @RequestBody NewPasswordRequest newPasswordRequest) {


		if(userRepository.existsByEmail(newPasswordRequest.getEmail())){
			
		  User user = userRepository.findByEmail(newPasswordRequest.getEmail());
		  user.setPassword(encoder.encode(newPasswordRequest.getPassword()));
		  userRepository.save(user);

		}

		return ResponseEntity.ok(new MessageResponse("User Password Edited successfully!"));
	}

	
}
