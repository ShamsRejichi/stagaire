package com.bezkoder.spring.jwt.mongodb.repository;

import java.util.List;
import java.util.Optional;

import com.bezkoder.spring.jwt.mongodb.models.CompteRendu;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface CompteRenduRepository extends MongoRepository<CompteRendu, String> {
    Optional<CompteRendu> findByFirstName(String firstName);
    Optional<CompteRendu> findByLastName(String lastName);
    List<CompteRendu> findByEmail(String email);
  
    List<CompteRendu> findAll();
  
    void deleteById(int id);
    
    Boolean existsByFirstName(String firstName);
    Boolean existsByLastName(String lastName);
  
    Boolean existsByEmail(String email);
  }