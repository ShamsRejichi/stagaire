# BesoinCompteRenduApp

 BesoinCompteRenduApp est une application web permet l'administration de gérer les consultants au sien de la société, ce dernier reçoit un email pour saisir son compte rendu.


## Tech stack
- Angular CLI: 9.1.5
- Node: 14.17.3
- OS: win32 x64
- TypeScript
- Angular Material UI component library

## Architecture

![architecture](https://angular.io/generated/images/guide/architecture/overview2.png)


This is a quick-start base for java projects with Spring Boot, MongoDB and configured JWT security.


## Running

Démarrez le MongoDB service/daemon sur votre système
Exécuter le projet par commande :

```bash
mvn spring-boot:run
```


## Improvements
- En cas de non saisie du CR, un mail de rappel sera envoyé au consultant au bout de 24 h.
- Chaque utilisateur aura la main pour apporter des modifications aux CR datant d’au moins de 4 jours. Au Delà de 4 jours, une autorisation de l’administrateur est obligatoire.