# BesoinCompteRenduApp

 BesoinCompteRenduApp est une application web permet l'administration de gérer les consultants au sien de la société, ce dernier reçoit un email pour saisir son compte rendu.


## Tech stack
- Angular CLI: 9.1.5
- Node: 14.17.3
- OS: win32 x64
- TypeScript
- Angular Material UI component library

## Architecture

![architecture](https://angular.io/generated/images/guide/architecture/overview2.png)


##Install the Angular CLI

Pour installer Angular CLI, ouvrez une fenêtre de terminal et exécutez la commande suivante :

```bash
npm install -g @angular/cli
```

##Install Angular Material

Utilisez le schéma d'installation de l'Angular CLI pour configurer votre projet Angular Material en exécutant la commande suivante :

```bash
ng add @angular/material
```

## Improvements
- En cas de non saisie du CR, un mail de rappel sera envoyé au consultant au bout de 24 h.
- Chaque utilisateur aura la main pour apporter des modifications aux CR datant d’au moins de 4 jours. Au Delà de 4 jours, une autorisation de l’administrateur est obligatoire.