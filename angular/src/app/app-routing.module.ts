import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/home/home.component';
import { AuthGuard } from './CanActivate';
import { FormConsultantComponent } from './form-consultant/form-consultant.component';
import { HistoryComponent } from './history/history.component';
import { LoginComponent } from './login/login.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { RegisterComponent } from './register/register.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';


const routes: Routes = [
 { path: 'home', component: HomeComponent, canActivate: [AuthGuard],  data: { role: 'ROLE_ADMIN'}  },
 // { path: 'home', component: HomeComponent },
   {path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },  
  { path: 'fromConsultant', component: FormConsultantComponent },
  { path: 'resetPassword', component: ResetpasswordComponent },
  { path: 'newPassword', component: NewPasswordComponent },
  { path: 'history', component: HistoryComponent },



];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    HttpClientModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }

