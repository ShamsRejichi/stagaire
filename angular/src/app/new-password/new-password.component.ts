import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {

  email:string;
  newpassword:string;
  confirmpassword:string;

  constructor(
    private snackBar:MatSnackBar,
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }


  newPassword(){
    this.authenticationService.newPassword(this.email, this.confirmpassword).subscribe((result)=> {
      this.snackBar.open('Reset Successful','',{duration:1000})
      this.router.navigate(['login']);

    }, () => {
      this.snackBar.open('Reset error','',{duration:1000})
    });     
  }
}
