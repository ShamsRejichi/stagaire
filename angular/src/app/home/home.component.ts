import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AuthenticationService } from '../auth.service';
import { User } from '../Models';
import { MatDialog , MatDialogConfig } from '@angular/material/dialog';
import { ConsultantComponent } from '../consultant/consultant.component';
import { Router } from '@angular/router';
import { ConfirmdialogComponent, ConfirmDialogModel } from '../confirmdialog/confirmdialog.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
  ROLES = 'roles'

  @Input() isMenuOpened: boolean;
  searchKey: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private dialog:MatDialog) { }

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['email' , 'firstname', 'lastname','x','y','z','w','actions']
  array = []

  userName :String
  role :String
  
  ngOnInit(): void {
    this.authenticationService.getusers().subscribe(
        (data:User[]) => {
          this.array = data
          console.log(this.array)
          this.listData = new MatTableDataSource(this.array);
          console.log( this.listData)
        })

        this.userName = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
        
        this.role = localStorage.getItem(this.ROLES)
  }
  
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase()
  }
  onCreate(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true ;
    dialogConfig.autoFocus = true ;
    dialogConfig.width = "40%"; 
    this.dialog.open(ConsultantComponent,dialogConfig)
  }


  onEdit(row){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "40%";
    this.dialog.open(ConsultantComponent,dialogConfig);
    this.authenticationService.setEdit(true)
  }
  Onclear(){}
  
  onDelete(row){
    const message = 'Are you sure to delete this Consultant ?';

    const dialogData = new ConfirmDialogModel("Confirm Action", message);

    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      this.authenticationService.Deleteconsultant(row.email)
      window.location.reload();
    });
}

  logout(){
    this.authenticationService.logout()
    this.router.navigate(['/']);

  }

  onSend(row){
    this.authenticationService.sendMailConsultant(row.email,row.firstName,row.lastName)
    
  }
  

  public openMenu(): void {
    this.isMenuOpened = !this.isMenuOpened;
  }

}
