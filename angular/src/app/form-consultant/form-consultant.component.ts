import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth.service';
import { MatSelect } from '@angular/material/select';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-form-consultant',
  templateUrl: './form-consultant.component.html',
  styleUrls: ['./form-consultant.component.css']
})
export class FormConsultantComponent implements OnInit {

  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
  ROLES = 'roles'

  userName :String
  role :String

  constructor(private authenticationService: AuthenticationService,
    private snackBar:MatSnackBar,
    private router: Router,) { }

  ngOnInit(): void {

    this.userName = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
        
    this.role = localStorage.getItem(this.ROLES)
  }

  compteRenduForm = new FormGroup({
    clientName: new FormControl('',Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    post: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    adress: new FormControl('', Validators.required),
    numero: new FormControl('', Validators.required),
    date : new FormControl('', Validators.required),
    interShipDuration: new FormControl('', Validators.required),
    numberOfParticipant : new FormControl('', Validators.required),
    deroulementInterShip : new FormControl('', Validators.required),
    ressenti : new FormControl('', Validators.required),
  });

  

  initializeFormGroup() {
    this.compteRenduForm.setValue({
      clientName: '',
      firstName: '',
      lastName: '',
      post: '',
      email: '',
      adress: '',
      number:'',
      date :'',
      interShipDuration: '',
      numberOfParticipant : '',
      deroulementInterShip : '',
      ressenti : '',
    });
  }
  
  logout(){
    this.authenticationService.logout()
    this.router.navigate(['/']);

  }

  
  onClear() {
    this.initializeFormGroup();
  }

  onSubmit() {

      this.authenticationService.addCompteRendu(
        this.compteRenduForm.value.clientName,
        this.compteRenduForm.value.firstName,
        this.compteRenduForm.value.lastName,
        this.compteRenduForm.value.post,
        this.compteRenduForm.value.numero,
        this.compteRenduForm.value.email,
        this.compteRenduForm.value.adress,
        this.compteRenduForm.value.interShipDuration,
        this.compteRenduForm.value.numberOfParticipant,
        "",
        "",
        "",
        this.compteRenduForm.value.date
        ).subscribe((result)=> {
        this.snackBar.open('Add Consultant Successful','',{duration:1000})
        window.location.reload();

    },error =>{console.log(error)})
 
  } 


  history(){
    
    this.router.navigate(['/history']);
  }
  

}
  