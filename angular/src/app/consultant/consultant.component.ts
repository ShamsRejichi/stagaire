import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth.service';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-consultant',
  templateUrl: './consultant.component.html',
  styleUrls: ['./consultant.component.css']
})
export class ConsultantComponent implements OnInit {

  dialogRef :MatDialogRef<ConsultantComponent>;
  constructor(private authenticationService: AuthenticationService,
    
    private snackBar:MatSnackBar,) { }

  ngOnInit(): void {
  }

  profileForm = new FormGroup({
    $key: new FormControl(null),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    poste: new FormControl('', Validators.required),
    experience: new FormControl('', Validators.required),
    adresse: new FormControl('', Validators.required),
    numero: new FormControl('', Validators.required)
  });

  

  initializeFormGroup() {
    this.profileForm.setValue({
      $key: null,
      firstName: '',
      lastName: '',
      email: '',
      poste: '',
      experience: '',
      adresse: '',
      numero: 0,
    });
  }

   onClear() {
    this.initializeFormGroup();
  }

  onSubmit() {
    if (this.profileForm.valid && !this.authenticationService.getEdit()) {
      this.authenticationService.addService(this.profileForm.value.firstName, this.profileForm.value.lastName ,this.profileForm.value.email ,this.profileForm.value.poste ,this.profileForm.value.experience,this.profileForm.value.numero,this.profileForm.value.adresse).subscribe((result)=> {
        this.snackBar.open('Add Consultant Successful','',{duration:1000})
        window.location.reload();

    })
    }else{
      if (this.profileForm.valid && this.authenticationService.getEdit()) {
        this.authenticationService.setEdit(false);
        this.authenticationService.updateServic(this.profileForm.value.firstName, this.profileForm.value.lastName ,this.profileForm.value.email,this.profileForm.value.poste ,this.profileForm.value.experience,this.profileForm.value.numero,this.profileForm.value.adresse)
          this.snackBar.open('update Consultant Successful','',{duration:1000});
          window.location.reload();
  
      
    }else{
      this.snackBar.open('Add Consultant Fail','',{duration:1000});
    }
  } 
}

  onClose(){
    this.initializeFormGroup();
    this.dialogRef.close()
  }
  
 
  
}
