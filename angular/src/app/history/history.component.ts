import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../auth.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
  ROLES = 'roles'
  EMAIL = 'email'

  userName :String
  role :String

  email :String
  adress: any;

  constructor(private authenticationService: AuthenticationService,
    private router: Router,) { }

  ngOnInit(): void {

    this.userName = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
        
    this.role = localStorage.getItem(this.ROLES)
    this.authenticationService.getCompteRendu(localStorage.getItem(this.EMAIL)).pipe(map((res:any) => {
      this.email = res[0].email ;
      this.adress = res[0].adress ;
      console.log(this.email);
      
     })).subscribe((result)=> {

  },error =>{console.log(error)})

  }
  
  logout(){
    this.authenticationService.logout()
    this.router.navigate(['/']);

  }

  history(){
    
    this.router.navigate(['/history']);
  }
  

}
  