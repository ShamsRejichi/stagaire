import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { User } from './Models';
import { Observable, Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  // BASE_PATH: 'http://localhost:8080'
  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
  TOKEN_SESSION_ATTRIBUTE_NAME = 'authenticatedtoken'
  ROLES = 'roles'
  EMAIL = 'email'

  public username: String;
  public password: String;
  public token: String;
  public roles: [];
  array=[]
  user:User
  isEdit: boolean=false ;

  constructor(private http: HttpClient) {

  }

  authenticationService(email: String, password: String) {
    return this.http.post(`http://localhost:8090/api/auth/signin`,JSON.stringify(
      {
        "email": email,
        "password": password
      }
    ),{ headers: { 'content-type': 'application/json'} }).pipe(map((res:any) => {
      console.log('roles',res.roles)
        this.username = res.username;
        this.password = password;
        this.token = res.accessToken
        this.roles = res.roles
        this.registerSuccessfulLogin(res.username, password,this.token,this.roles,res.email);
      }));
  }

  registerService(username: String, password: String, email : String) {
    return this.http.post(`http://localhost:8090/api/auth/signup`,JSON.stringify(
      {
        "username": username,
        "email" : email,
        "password": password,
        "roles" : ["user"]
      }
    ),{ headers: { 'content-type': 'application/json'} }).pipe(map((res:any) => {
      res
      }));
  }

  resetPassword(email: string) {
    console.log(email)
    return this.http.post(`http://localhost:8090/api/auth/resetPassword`,JSON.stringify(
      {
        "email" : email
      }
    ),{ headers: { 'content-type': 'application/json'} }).pipe(map((res:any) => {
      res
      }));
  }

  newPassword(email: string, password : string) {
    return this.http.post(`http://localhost:8090/api/auth/newpassword`,JSON.stringify(
      {
        "password": password,
        "email" : email
      }
    ),{ headers: { 'content-type': 'application/json'} }).pipe(map((res:any) => {
      res
      }));
  }

  addService(firstName: String,lastName: String, email : String, poste : String, experience: String,numero: String,adresse:String) {
    return this.http.post(`http://localhost:8090/api/auth/addConsultant`,JSON.stringify(
      {
        "firstName": firstName,
        "lastName": lastName,
        "email" : email,
        "poste" : poste,
        "experience" : experience,
        "numero" : numero,
        "adresse" : adresse,
      }
    ),{ headers: { 'content-type': 'application/json'} }).pipe(map((res:any) => {
      res
      }));
  }

  

  updateServic(firstName: String,lastName: String, email : String, poste : String, experience: String,numero: String,adresse:String) {
    return this.http.post(`http://localhost:8090/api/test/updateConsultant`,JSON.stringify(
      {
        "firstName": firstName,
        "lastName": lastName,
        "email" : email,
        "poste" : poste,
        "experience" : experience,
        "numero" : numero,
        "adresse" : adresse,
      }
    ),{ headers: { 'content-type': 'application/json' , 'Authorization' : 'Bearer ' + this.token} }).subscribe(res => {},error =>{console.log(error)});
  }


  
  getRole():string {
     return localStorage.getItem(this.ROLES);
   }

   getRules():string[]{
    return this.roles;
  }

  getusers() {  
    return this.http.get(`http://localhost:8090/api/test/users`,{ headers: { 'content-type': 'application/json', 'Authorization' : 'Bearer ' + this.token} });
  }

  createBasicAuthToken(username: String, password: String) {
    return 'Basic ' + window.btoa(username + ":" + password)
  }

  registerSuccessfulLogin(username, password,token,roles,email) {
    localStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, username)
    localStorage.setItem(this.TOKEN_SESSION_ATTRIBUTE_NAME, token)
    localStorage.setItem(this.ROLES ,roles[0])
    localStorage.setItem(this.EMAIL ,email)
    
  }

  logout() {
    localStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    localStorage.removeItem(this.TOKEN_SESSION_ATTRIBUTE_NAME);
    localStorage.removeItem(this.ROLES);
    localStorage.removeItem(this.EMAIL);
    this.username = null;
    this.password = null;
    this.token = null;

  }

  isUserLoggedIn() {
    let user = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    console.log("user",user)
    if (user !== null) return true
    return false
  }

  getLoggedInUserName() {
    let user = localStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return ''
    return user
  }

  
  Deleteconsultant(email: string) {
    console.log(email)
     return this.http.post(`http://localhost:8090/api/test/delete`,JSON.stringify(
      {
        "email" : email,
      }
    ),{ headers: { 'content-type': 'application/json' , 'Authorization' : 'Bearer ' + this.token} }).subscribe(res => {},error =>{console.log(error)});
    
  }


  sendMailConsultant(email: string,firstName:string,lastName:string) {
    console.log(email)
    console.log(firstName)
    console.log(lastName)

     return this.http.post(`http://localhost:8090/api/test/sendMailConsultant`,JSON.stringify(
      {
        "email" : email,
        "firstName" : firstName,
        "lastName" : lastName
      }
    ),{ headers: { 'content-type': 'application/json' , 'Authorization' : 'Bearer ' + this.token} }).subscribe(res => {},error =>{console.log(error)});
    
  }
  
  setEdit(isEdit: boolean) {
    this.isEdit = isEdit
  }

  getEdit() {
    return this.isEdit 
  }

  
  addCompteRendu(clientName: string,firstName:string,lastName:string,poste:string,numero:string,email:string,adress: string,intershipDuration:string,numberOfParticipant:string,intershipBTW:string,intershipType: string,positionnement:string,date:Date) {

     return this.http.post(`http://localhost:8090/api/test/addCompteRendu`,JSON.stringify(
      {
        "clientName" : clientName,
        "firstName" : firstName,
        "lastName" : lastName,
        "poste" : poste,
        "numero" : numero,
        "email" : email,
        "adress" : adress,
        "intershipDuration" : intershipDuration,
        "numberOfParticipant" : numberOfParticipant ,
        "intershipBTW" : intershipBTW,
        "intershipType" : intershipType, 
        "positionnement" : positionnement,
        "date" : date
      }
    ),{ headers: { 'content-type': 'application/json' , 'Authorization' : 'Bearer ' + this.token} }).pipe(map((res:any) => {
      res
      }));
    
  }


  
  
  getCompteRendu(email:string) {

    return this.http.post(`http://localhost:8090/api/test/getCompteRendu`,JSON.stringify(
     {
       "email" : email,
     }
   ),{ headers: { 'content-type': 'application/json'} });
}

  

}
