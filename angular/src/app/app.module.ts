import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {  MatTableModule } from "@angular/material/table";
import { ConsultantComponent } from './consultant/consultant.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list';
import { ReactiveFormsModule } from '@angular/forms';
import { FormConsultantComponent } from './form-consultant/form-consultant.component';
import {MatSelectModule} from '@angular/material/select';
import { HomeComponent } from './home/home.component';
import {  MatNativeDateModule } from '@angular/material/core';
import {  MatDatepickerModule } from '@angular/material/datepicker';
import { ConfirmdialogComponent } from './confirmdialog/confirmdialog.component';
import { MatMenuModule } from '@angular/material/menu';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { HistoryComponent } from './history/history.component';







@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ConsultantComponent,
    LoginComponent,
    RegisterComponent,
    ResetpasswordComponent,
    ConsultantComponent,
    FormConsultantComponent,
    ConfirmdialogComponent,
    ResetpasswordComponent,
    NewPasswordComponent,
    HistoryComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatTableModule,
    MatSnackBarModule,
    MatDialogModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatButtonModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatMenuModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ConsultantComponent]
})
export class AppModule { }
