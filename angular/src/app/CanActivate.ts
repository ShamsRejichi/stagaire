import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './auth.service';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.authenticationService.isUserLoggedIn()) {
        const userRole = this.authenticationService.getRole();
        console.log(route.data.role.indexOf(userRole))
        if (route.data.role && route.data.role.indexOf(userRole) === -1) {
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      }
      this.router.navigate(['/']);
      return false;
    }
}