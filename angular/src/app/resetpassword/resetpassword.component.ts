import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  email:string;

  constructor(
    private snackBar:MatSnackBar,
    private router: Router,
    private authenticationService: AuthenticationService){
 }

  ngOnInit(): void {
  }

  reset(){
    this.authenticationService.resetPassword(this.email).subscribe((result)=> {
      this.snackBar.open('Reset Successful','',{duration:1000})
      this.router.navigate(['login']);

    }, () => {
      this.snackBar.open('Reset error','',{duration:1000})
    });      
  }

}
