import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class  LoginComponent {
  title = 'demoApp';
 email:string;
 password:string;
 remail:string;
 rpassword:string;
 rcpassword:string;

  constructor(
    private snackBar:MatSnackBar,
    private router: Router,
    private authenticationService: AuthenticationService){

  }

  
  ngOnInit(): void {
  }
 
  login() {
    this.authenticationService.authenticationService(this.email, this.password).subscribe((result)=> {
      this.snackBar.open('Login Successful','',{duration:1000})
      if(this.authenticationService.getRules().includes("ROLE_ADMIN")){
        this.router.navigate(['/home']);
      }else{
        this.router.navigate(['/fromConsultant']);
      }
    

    }, () => {
      this.snackBar.open('Login error','',{duration:1000})
    });      
  }

  register(){
    
    this.router.navigate(['/register']);
  }

  restPassword(){
    this.router.navigate(['/resetPassword']);

  }
}
