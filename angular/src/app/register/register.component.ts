import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  title = 'demoApp';
  email:string;
  password:string;
  remail:string;
  rpassword:string;
  rcpassword:string;
 
   constructor(
     private snackBar:MatSnackBar,
     private router: Router,
     private authenticationService: AuthenticationService){
 
   }

  register() {
    this.authenticationService.registerService(this.rpassword, this.rcpassword ,this.remail , ).subscribe((result)=> {
      this.snackBar.open('Register Successful','',{duration:1000})
      this.router.navigate(['login']);

    }, () => {
      this.snackBar.open('Register error','',{duration:1000})
    });      

  }

}
